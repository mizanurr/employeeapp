﻿using CommunityToolkit.Maui;
using MauiApp1.Pages;
using MauiApp1.Services;
using MauiApp1.ViewModels;
using Microsoft.Extensions.Logging;
using UraniumUI;
namespace MauiApp1
{
    public static class MauiProgram
    {
        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseMauiCommunityToolkit()
                .UseUraniumUI()
                .UseUraniumUIMaterial()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                    fonts.AddMaterialIconFonts();
                });
            builder.Services.AddCommunityToolkitDialogs();


#if DEBUG
            builder.Logging.AddDebug();
#endif
            builder.Services.AddSingleton<LeaveAppViewModel>();
         //   builder.Services.AddTransient<LeaveApplicationService>();
            builder.Services.AddSingleton<LeaveApplicationPage>();
            builder.Services.AddSingleton<ApprovalPage>();
            return builder.Build();
        }
    }
}
