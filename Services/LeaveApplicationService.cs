﻿using MauiApp1.Models;

namespace MauiApp1.Services;

public class LeaveApplicationService
{
    public List<LeaveApp> LeaveApps { get; set; }

    public LeaveApplicationService()
    {
        LeaveApps = [
            new() { Id = 1, LeaveType = "Casual", FromDate = Convert.ToDateTime("12-11-2023"), ToDate = DateTime.Now, LeaveReason = "test leave Reasons", LeaveLocation = "Dhaka", LeaveStatus = "Pending" },
            new() { Id = 2, LeaveType = "Yearly", FromDate = Convert.ToDateTime("11-11-2023"), ToDate = DateTime.Today, LeaveReason = "test leave Reasons 2", LeaveLocation = "Chittagong", LeaveStatus = "Rejected" },
            new() { Id = 3, LeaveType = "Sick", FromDate = Convert.ToDateTime("09-10-2023"), ToDate = DateTime.Now, LeaveReason = "test leave Reasons 3", LeaveLocation = "Khulna", LeaveStatus = "Approved" },
            new() { Id = 4, LeaveType = "Casual", FromDate = Convert.ToDateTime("12-02-2023"), ToDate = DateTime.Now, LeaveReason = "test leave Reasons 4", LeaveLocation = "Sylhet", LeaveStatus = "Pending" },
            new() { Id = 5, LeaveType = "Sick", FromDate = Convert.ToDateTime("12-02-2023"), ToDate = DateTime.Today, LeaveReason = "test leave Reasons 4", LeaveLocation = "Sylhet", LeaveStatus = "Pending" },
        ];
    }

    public  void CreateLeave (LeaveApp leaveApp) => LeaveApps.Add(leaveApp);
    public  void UpdateLeave (LeaveApp leaveApp)
    {
      var updateableLeave = LeaveApps.Where(l => l.Id == leaveApp.Id).FirstOrDefault();
        if (updateableLeave != null)
        {
            updateableLeave.LeaveType = leaveApp.LeaveType;
            updateableLeave.FromDate = leaveApp.FromDate;
            updateableLeave.ToDate = leaveApp.ToDate;
            updateableLeave.LeaveLocation = leaveApp.LeaveLocation;
            updateableLeave.LeaveReason = leaveApp.LeaveReason;
            updateableLeave.LeaveStatus = leaveApp.LeaveStatus;
        }
    }
    public  void DeleteLeave (LeaveApp leaveApp) => LeaveApps.Remove(leaveApp);
}
