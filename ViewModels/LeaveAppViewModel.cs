﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MauiApp1.Models;
using MauiApp1.Services;
namespace MauiApp1.ViewModels;


public partial class LeaveAppViewModel : ObservableObject
{
    public LeaveApplicationService leaveService;
    public LeaveAppViewModel()
    {
        leaveService = new LeaveApplicationService();
        LeaveApps = leaveService.LeaveApps;
    }

    [ObservableProperty]
    private List<LeaveApp> _leaveApps;

    [ObservableProperty]
    private bool isRefreshing;



    [RelayCommand]
    private void Refresh()
    {
        IsRefreshing = true;
        Task.Delay(1000);
        IsRefreshing = false;
    } 
    
    [RelayCommand]
    private void Delete(Object s)
    {
        var ss = s;
        //leaveService.DeleteLeave()
    }

    public void AddLeaveApplication(LeaveApp leaveApp)
    {
        leaveService.CreateLeave(leaveApp);
    }
}
