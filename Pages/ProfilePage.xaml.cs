using MauiApp1.Views;

namespace MauiApp1.Pages;

public partial class ProfilePage : ContentPage
{
	public ProfilePage()
	{
		InitializeComponent();
        //Routing.RegisterRoute(nameof(PersonalView), typeof(PersonalView));
        Offical.IsVisible = false;

    }

    private void btnPersonal_Clicked(object sender, EventArgs e)
    {
        // Shell.Current.GoToAsync(nameof(PersonalView));
        // PerOfficeTabContentView.AddLogicalChild(new PersonalView());
        Offical.IsVisible = false;
       personal.IsVisible = true;
    }

    private void btnOfficial_Clicked(object sender, EventArgs e)
    {
        personal.IsVisible = false;
        Offical.IsVisible = true;
       
    }
}