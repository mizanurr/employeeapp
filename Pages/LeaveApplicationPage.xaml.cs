using CommunityToolkit.Maui.Views;
using MauiApp1.Models;
using MauiApp1.ViewModels;

namespace MauiApp1.Pages;

public partial class LeaveApplicationPage : ContentPage
{
    private readonly LeaveAppViewModel viewModel;
    public LeaveApplicationPage(LeaveAppViewModel leaveAppViewModel)
    {
        InitializeComponent();
        viewModel = leaveAppViewModel;
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        LeaveAppCollection.ItemsSource = viewModel.LeaveApps;
    }

    private async void ApplyLeave_Taped(object sender, EventArgs e)
    {
        // await  Shell.Current.Navigation.PushModalAsync( new ApprovalPage());

        // await  Shell.Current.GoToAsync(nameof(LeaveApplyPage), true);

        var result = await this.ShowPopupAsync(new LeaveApplyPage());
        if (result is not null)
        {
            viewModel.AddLeaveApplication((LeaveApp)result);
        }
    }
}