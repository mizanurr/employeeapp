using MauiApp1.ViewModels;
using Microsoft.Maui.Controls;

namespace MauiApp1.Pages;

public partial class ApprovalPage : ContentPage
{
    private readonly LeaveAppViewModel viewModel;

    public ApprovalPage(LeaveAppViewModel ViewModel)
	{
		InitializeComponent();
        BindingContext = viewModel = ViewModel;
    }

    protected override void OnAppearing()
    {
        base.OnAppearing();
        LeaveAppCollection.ItemsSource = viewModel.LeaveApps;
    }
}