using CommunityToolkit.Maui.Views;
using MauiApp1.Models;

namespace MauiApp1.Pages;

public partial class LeaveApplyPage : Popup
{
    public LeaveApplyPage()
    {
        InitializeComponent();

    }

    private void BtnApply_Clicked(object sender, EventArgs e)
    {
        var model = new LeaveApp
        {
            Id = 6,
            LeaveType = LeaveType.SelectedItem.ToString()?? "Sick",
            LeaveLocation = LeaveLocation.Text,
            LeaveReason = LeaveReason.Text,
            LeaveStatus = "Pending",
            FromDate = FromDate.Date?? DateTime.Now,
            ToDate = ToDate.Date?? DateTime.Now,
        };
        Close(model);
    }


    private async void BtnCancel_Clicked(object sender, EventArgs e)
    {
      await  CloseAsync();
    }
}