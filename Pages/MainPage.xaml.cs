﻿namespace MauiApp1.Pages;

public partial class MainPage : ContentPage
{
    int count = 0;

    public MainPage()
    {
        InitializeComponent();
        Routing.RegisterRoute(nameof(ProfilePage), typeof(ProfilePage));
        Routing.RegisterRoute(nameof(AttendencePage), typeof(AttendencePage));
        Routing.RegisterRoute(nameof(LeaveApplicationPage), typeof(LeaveApplicationPage));
        Routing.RegisterRoute(nameof(ApprovalPage), typeof(ApprovalPage));
    }

    private async void Profile_Tapped(object sender, TappedEventArgs e)
    {
       await Shell.Current.GoToAsync(nameof(ProfilePage));
    }  
    
    private async void Attendence_Tapped(object sender, TappedEventArgs e)
    {
       await Shell.Current.GoToAsync(nameof(AttendencePage));
    }  
    private async void LeaveApp_Tapped(object sender, TappedEventArgs e)
    {
       await Shell.Current.GoToAsync(nameof(LeaveApplicationPage));
    }  
    private async void Approval_Tapped(object sender, TappedEventArgs e)
    {
      await  Shell.Current.GoToAsync(nameof(ApprovalPage));
    }
}
