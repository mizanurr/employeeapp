﻿namespace MauiApp1.Models;

public class LeaveApp
{
    public int Id { get; set; }
    public required string LeaveType { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string? LeaveReason { get; set; }
    public string? LeaveLocation { get; set; }
    public required string LeaveStatus { get; set; }
}
